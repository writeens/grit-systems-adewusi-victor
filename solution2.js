/* eslint-disable prefer-const */
function reverseBits(num) {
    let newBinary = [];
    let bits = num;
    bits = parseInt(num, 10).toString(2);
    let myArray = bits.split("");
    let excess = 32 - bits.length;
    if (excess < 32) {
        for (let i = 0; i < excess; i += 1) {
            myArray.unshift("0");
        }
    }
    for (let i = myArray.length; i >= 0; i -= 1) {
        newBinary.push(myArray[i]);
    }
    let finalBinary = newBinary.join("").toString(10);
    let finalNumber = parseInt(finalBinary, 2);
    return finalNumber;
}
console.log(reverseBits(43261596));
