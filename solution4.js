/* eslint-disable prefer-const */
function fib(num) {
    let limit = num;
    let myArray = [0, 1];
    if (limit === 0) return 0;
    if (limit === 1) return 1;
    for (let i = 2; i < limit; i += 1) {
        myArray[i] = myArray[i - 2] + myArray[i - 1];
    }
    myArray = myArray.filter(item => item < limit);
    return myArray;
}
console.log(fib(3));
